
// Implémenter ici les fonctions paint à ajouter dans chacune des classes du modèle.
Rectangle.prototype.paint = function (ctx) {
    console.log(this.initialX);
    console.log(this.initialY);
    console.log(this.finalX);
    console.log(this.finalY);
    ctx.rect(this.initialX, this.initialY, this.finalX, this.finalY);
    ctx.lineWidth = this.thickness;
    ctx.strokeStyle = this.color;
    ctx.stroke();
};

Line.prototype.paint = function (ctx) {
    ctx.lineWidth = this.thicknes;
    ctx.strokeStyle = this.color;
    ctx.beginPath();
    ctx.moveTo(this.initialX, this.initialY);
    ctx.lineTo(this.finalX, this.finalY);
    ctx.stroke();
};

Drawing.prototype.paint = function (ctx) {
    ctx.fillStyle = '#F0F0F0';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    this.tabForm.forEach(function (eltDuTableau) {
        eltDuTableau.paint(ctx);
    });
};

Form.prototype.clear = function(ctx) {
    canvas.getContext('2d').fillStyle = '#F0F0F0'; // set canvas' background color
    ctx.fillRect(0, 0, canvas.width, canvas.height);
  };
