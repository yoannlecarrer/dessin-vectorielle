
//var editingMode = { rect: 0, line: 1 };

let spinner = null;
let color = null;
let button = null

class Pencil{

	constructor(ctx, drawing, canvas){

		// Liez ici les widgets à la classe pour modifier les attributs présents ci-dessus.
		spinner = document.querySelector('#spinnerWidth');
		if(!spinner){
			alert("#spinnerWidth not found");
			return;
		}

		color = document.querySelector('#colour');
		if(!color){
			alert("#colour not found");
			return;
		}

		button = document.querySelector('#butRect');
		if(!butRect){
			alert("#butRect not found");
			return;
		}

		this.currentShape = 0;
		this.ctx = ctx;
		this.drawing = drawing;
		this.canvas = canvas;
		this.currLineWidth = spinner.value;
		this.currColour = color.value;
		
		new DnD(canvas, this);

	}

	// Implémentez ici les 3 fonctions onInteractionStart, onInteractionUpdate et onInteractionEnd
	onInteractionStart = (dnd) => {
		if(button.checked){
			this.currentShape = new Rectangle(dnd.initialX,dnd.initialY,dnd.finalX,dnd.finalY,spinner.value, color.value);
		}
		else{
			this.currentShape = new Line(dnd.initialX,dnd.initialY,dnd.finalX,dnd.finalY,spinner.value, color.value);
		}
	}

	onInteractionUpdate = (dnd) => {
		if (this.currentShape != 0){
			this.currentShape.clear(this.ctx)
			this.currentShape.finalX = dnd.finalX;
			this.currentShape.finalY = dnd.finalY;
			this.drawing.paint(this.ctx);
			this.currentShape.paint(this.ctx);
		}
	}

	onInteractionEnd = (dnd) => {
		this.currentShape.finalX = dnd.finalX;
		this.currentShape.finalY = dnd.finalY;
		this.currentShape.paint(this.ctx);
		this.drawing.addForms(this.currentShape);
		this.drawing.paint(this.ctx);
	}
};


