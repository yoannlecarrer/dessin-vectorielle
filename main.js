const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');

canvas.width=800
canvas.height=600

ctx.fillStyle = 
ctx.fillRect(0, 0, canvas.width, canvas.height);  

var drawing = new Drawing();
var pencil = new Pencil(ctx, drawing, canvas);
drawing.paint(ctx, canvas);

