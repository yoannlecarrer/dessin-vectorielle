
// Implémenter ici les 4 classes du modèle.
// N'oubliez pas l'héritage !

class Drawing {
    constructor() {
        this.tabForm = new Array();
    }

    addForms = (form) => {
        this.tabForm.push(form);
    }

}

class Form {
    constructor(initialX, initialY, finalX, finalY,thickness, color) {
        this.initialX = initialX;
        this.initialY = initialY;
        this.finalX = finalX;
        this.finalY = finalY;
        this.thickness = thickness;
        this.color = color;
    }
}

class Rectangle extends Form {
    constructor(initialX, initialY, finalX, finalY, thickness, color) {
        super(initialX, initialY, finalX, finalY,thickness, color)
    }
}

class Line extends Form {
    constructor(initialX, initialY, finalX, finalY, thickness, color) {
        super(initialX, initialY, finalX, finalY,thickness, color)
    }
}