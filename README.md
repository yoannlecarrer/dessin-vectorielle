# TP Web : Javascript et HTML5

L’objectif de ce TP consistait à créer une application Web pour faire du dessin vectoriel, i.e. pour dessiner des rectangles, des lignes, ainsi que définir leur couleur et leur épaisseur de trait. 

### Branche de développement 
Nous avons tout mis sur une seul branche "Master"

### Processus de lancement du TP
Pour faire fonctionner le tp il suffit de lancer la page cavans.html dans un navigateur 
Le Javascript a été codé en utilisant la norme ES6 (ECMAScript 2015 - ES6)


