
// La création d'un Dnd requière un canvas et un interacteur.
// L'interacteur viendra dans un second temps donc ne vous en souciez pas au départ.

class DnD {

  constructor(canvas, interactor) {
    this.canvas = canvas;
    this.interactor = interactor;
    this.initialX = 0;
    this.initialY = 0;
    this.finalX = 0;
    this.finalY = 0;
    this.pression = false

    // Developper les 3 fonctions gérant les événements
    const mousseDown = (evt) => {
      this.pression = true;
      this.initialX = getMousePosition(this.canvas, evt).x;
      this.initialY = getMousePosition(this.canvas, evt).y;
      this.interactor.onInteractionStart(this);
    }

    const mousseMove = (evt) => {
      if (this.pression) {
        this.finalX = getMousePosition(this.canvas, evt).x;
        this.finalY = getMousePosition(this.canvas, evt).y;
        this.interactor.onInteractionUpdate(this);
      }
    }

    const mousseUp = (evt) => {
      this.pression = false
      this.finalX = getMousePosition(this.canvas, evt).x;
      this.finalY = getMousePosition(this.canvas, evt).y;
      this.interactor.onInteractionEnd(this);
    }

    // Associer les fonctions précédentes aux évènements du canvas.
    this.canvas.addEventListener('mousedown', (evt) => {
      mousseDown(evt);
    });

    this.canvas.addEventListener('mousemove', (evt) => {
      mousseMove(evt);
    });

    this.canvas.addEventListener('mouseup', (evt) => {
      mousseUp(evt);
    });
  }
}

// Place le point de l'événement evt relativement à la position du canvas.
const getMousePosition = (canvas, evt) => {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
};



